//
//  HouseholdAccountMO.h
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 30..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#ifndef HouseholdAccountMO_h
#define HouseholdAccountMO_h
#import <Foundation/Foundation.h>

@interface HouseholdAccountMO : NSObject

@property NSString * type;
@property NSDate * selectedDay;
@property NSString * classification;
@property NSNumber * money;

- (id) initWithType: (NSString *) type andDate: (NSDate *) day andClassify: (NSString *) category andMoney: (NSNumber *) money;
@end


#endif /* HouseholdAccountMO_h */
