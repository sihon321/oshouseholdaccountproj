//
//  OSHTableViewCell.h
//  CoreDataProj1
//
//  Created by sihon321 on 2017. 3. 27..
//  Copyright © 2017년 sihon321. All rights reserved.
//

#ifndef OSHTableViewCell_h
#define OSHTableViewCell_h
#import <UIKit/UIKit.h>

@interface OSHTableViewCell : UITableViewCell{
    
}
@property (retain, nonatomic) UILabel * typeLabel, * categoryLabel, * moneyLabel;

- (void) setCellDataFromType : (NSString *) type andCategory: (NSString *) category andMoney: (NSString *) money;
@end
#endif /* OSHTableViewCell_h */
