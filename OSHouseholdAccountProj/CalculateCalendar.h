//
//  CalculateCalendar.h
//  CalendarProj
//
//  Created by sihon321 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#ifndef CalculateCalendar_h
#define CalculateCalendar_h
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CalculateCalendar : NSObject{
    
}
@property NSInteger presentDay, presentWeekday, presentMonth, presentYear;
@property NSInteger moveDay, moveWeekday, moveMonth, moveYear;

- (NSArray *) setupWeeksArray;
- (void) loadCalendarFromYear: (NSInteger)year andMonth: (NSInteger)month;
- (NSInteger) getLastdayOfMonth;
- (void) initPresentValue: (NSDateComponents*) component;
@end

#endif /* CalculateCalendar_h */
