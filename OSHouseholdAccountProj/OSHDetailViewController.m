//
//  OSHDetailViewController.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 28..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "OSHDetailViewController.h"

@interface OSHDetailViewController ()

@property (strong, nonatomic) UITableView * tableView;
@property (strong, nonatomic) UIView * calculateView;
@property (strong, nonatomic) UILabel * monthExpenseLabel, * todayExpenseLabel;
@property (strong, nonatomic) UILabel * monthIncomeLabel, * todayIncomeLabel;

@property NSString * dateStr;
@property HouseholdAccountDAO * accountParser;
@property NSMutableArray * accountArray;
@property NSInteger monthExpense, todayExpense;
@property NSInteger monthIncome, todayIncome;

@end

static NSString * const kCellReuseIdentifier = @"OSHCellReuseIdentifier";
static NSString * const kExpenseType = @"expense";
static NSString * const kIncomeType = @"income";

@implementation OSHDetailViewController

#pragma mark - LifeCycle Methods

- (void) viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self caculateMonthAndTodayMoney];
    [self setupCalculateView];
    [self setupConstraint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup Methods

- (void) setupTableView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    _tableView = [UITableView new];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OSHTableViewCell class] forCellReuseIdentifier:kCellReuseIdentifier];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    
    [self.view addSubview:_tableView];
    _accountParser = [HouseholdAccountDAO new];
    [_accountParser loadHouseholdAccount];
    _accountArray = [_accountParser loadTodayHouseholdAccount:[self parseToDate]];
}

- (void) setupDate : (NSInteger) year Month : (NSInteger) month Day : (NSInteger) day {
    _yearStr = year;
    _monthStr = month;
    _dayStr = day;
}

- (void) setupCalculateView {
    _calculateView = [UIView new];
    _calculateView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_calculateView];
    
    _monthExpenseLabel = [UILabel new];
    _monthExpenseLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_monthExpenseLabel setText:[NSString stringWithFormat:@"월 지출 : %ld", _monthExpense]];
    [_calculateView addSubview:_monthExpenseLabel];
    
    _todayExpenseLabel = [UILabel new];
    _todayExpenseLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_todayExpenseLabel setText:[NSString stringWithFormat:@"일 지출 : %ld", _todayExpense]];
    [_calculateView addSubview:_todayExpenseLabel];
    
    _monthIncomeLabel = [UILabel new];
    _monthIncomeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_monthIncomeLabel setText:[NSString stringWithFormat:@"월 수입 : %ld", _monthIncome]];
    [_calculateView addSubview:_monthIncomeLabel];
    
    _todayIncomeLabel = [UILabel new];
    _todayIncomeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_todayIncomeLabel setText:[NSString stringWithFormat:@"일 수입 : %ld", _todayIncome]];
    [_calculateView addSubview:_todayIncomeLabel];
}

- (void) setupConstraint{
    NSDictionary *viewsDictionary = @{@"tableView":_tableView, @"calculateView":_calculateView, @"monthExpenseLabel":_monthExpenseLabel, @"weeksExpenseLabel":_todayExpenseLabel, @"monthIncomeLabel":_monthIncomeLabel, @"weeksIncomeLabel":_todayIncomeLabel};
    NSDictionary *metrics = @{@"vSpacing":@10,
                              @"hSpacing":@5,
                              @"eSpacing":@100,
                              @"calculateHeight":@100,
                              @"monthExpenseWidth":@150,
                              @"monthExpenseHeight":@44,
                              @"weeksExpenseHeight":@44,
                              @"weeksExpenseWidth":@150,
                              @"monthIncomeWidth":@150,
                              @"monthIncomeHeight":@44,
                              @"weeksIncomeWidth":@150,
                              @"weeksIncomeHeight":@44};

    NSArray * table_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[tableView]-eSpacing-|"
                                                                          options:0
                                                                          metrics:metrics
                                                                            views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:table_constraint_POS_V];
    
    NSArray * table_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[tableView]-hSpacing-|"
                                                                              options:0
                                                                              metrics:metrics
                                                                                views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:table_constraint_POS_H];
    
    NSArray * table_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[calculateView(calculateHeight)]"
                                                                           options:0
                                                                           metrics:metrics
                                                                             views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:table_constraint_V];
    NSArray * calculate_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[calculateView]-vSpacing-|"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:calculate_constraint_POS_V];
    NSArray * calculate_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[calculateView]-hSpacing-|"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:calculate_constraint_POS_H];
    
    
    NSArray * monthExpense_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[monthExpenseLabel(monthExpenseWidth)]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthExpense_constraint_H];
    NSArray * monthExpense_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[monthExpenseLabel(monthExpenseHeight)]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthExpense_constraint_V];
    NSArray * monthExpense_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[monthExpenseLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthExpense_constraint_POS_H];
    NSArray * monthExpense_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[monthExpenseLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthExpense_constraint_POS_V];
    
    
    NSArray * weeksExpense_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[weeksExpenseLabel(weeksExpenseWidth)]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksExpense_constraint_H];
    NSArray * weeksExpense_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[weeksExpenseLabel(weeksExpenseHeight)]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksExpense_constraint_V];
    NSArray * weeksExpense_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[weeksExpenseLabel]-hSpacing-|"
                                                                                     options:0
                                                                                     metrics:metrics
                                                                                       views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksExpense_constraint_POS_H];
    NSArray * weeksExpense_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[weeksExpenseLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksExpense_constraint_POS_V];

    
    NSArray * monthIncome_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[monthIncomeLabel(monthIncomeWidth)]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthIncome_constraint_H];
    NSArray * monthIncome_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[monthIncomeLabel(monthIncomeHeight)]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthIncome_constraint_V];
    NSArray * monthIncome_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[monthIncomeLabel]"
                                                                                     options:0
                                                                                     metrics:metrics
                                                                                       views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthIncome_constraint_POS_H];
    NSArray * monthIncome_constraint_POS_V  = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[monthExpenseLabel]-vSpacing-[monthIncomeLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:monthIncome_constraint_POS_V];
    
    
    NSArray * weeksIncome_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[weeksIncomeLabel(weeksIncomeWidth)]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksIncome_constraint_H];
    NSArray * weeksIncome_constraint_V= [NSLayoutConstraint constraintsWithVisualFormat:@"V:[weeksIncomeLabel(monthIncomeHeight)]"
                                                                                options:0
                                                                                metrics:metrics
                                                                                  views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksIncome_constraint_V];
    NSArray * weeksIncome_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[weeksIncomeLabel]-hSpacing-|"
                                                                                     options:0
                                                                                     metrics:metrics
                                                                                       views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksIncome_constraint_POS_H];
    NSArray * weeksIncome_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[weeksExpenseLabel]-vSpacing-[weeksIncomeLabel]"
                                                                                     options:0
                                                                                     metrics:metrics
                                                                                       views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:weeksIncome_constraint_POS_V];
}

#pragma mark - Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    return _accountArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OSHTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [OSHTableViewCell new];
    }
    HouseholdAccountMO * data = _accountArray[indexPath.row];
    [cell setCellDataFromType:data.type andCategory:data.classification andMoney:[NSString stringWithFormat:@"%@ 원", data.money]];
    
    return cell;
}

#pragma mark - Util Methods

- (NSDate *) parseToDate {
    NSDateFormatter * dateFormatter = [NSDateFormatter new];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ko"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDateComponents * dateComponents = [NSDateComponents new];
    [dateComponents setYear:_yearStr];
    [dateComponents setMonth:_monthStr];
    [dateComponents setDay:_dayStr];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate * date = [calendar dateFromComponents:dateComponents];

    return date;
}

- (void) caculateMonthAndTodayMoney {
    NSDate * today = [self parseToDate];
    NSArray * todayArr = [_accountParser loadTodayHouseholdAccount:today];
    NSArray * monthArr = [_accountParser loadTodayMonthMoney:today];
    _todayExpense = _todayIncome = _monthExpense = _monthIncome = 0;
    
    for(HouseholdAccountMO * data in todayArr)
    {
        if([data.type isEqualToString:kExpenseType]) {
            _todayExpense += [self NSnumberToNSinteger:data.money];
            NSLog(@"%ld",_todayExpense);
        } else {
            _todayIncome += [self NSnumberToNSinteger:data.money];
        }
    }
    
    for(HouseholdAccountMO * data in monthArr){
        if([data.type isEqualToString:kExpenseType]) {
            _monthExpense += [self NSnumberToNSinteger:data.money];
        } else {
            _monthIncome += [self NSnumberToNSinteger:data.money];
        }
    }
}

- (NSInteger)NSnumberToNSinteger:(NSNumber *)NSnumber
{
    NSInteger returnInteger = [NSnumber integerValue];
    return returnInteger;
}

@end
