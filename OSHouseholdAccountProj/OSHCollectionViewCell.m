//
//  OSHCollectionViewCell.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "OSHCollectionViewCell.h"

@interface OSHCollectionViewCell ()

@end


@implementation OSHCollectionViewCell

- (id) initWithFrame:(CGRect) frame;{
    
    self = [super initWithFrame:frame];
    if(self) {
        self.dayLabel = [[UILabel alloc] initWithFrame:self.contentView.bounds];
        self.dayLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.dayLabel];
    }
    return self;
}

- (void)setCellData:(NSString *)data {
    [self.dayLabel setText:data];
}
@end
