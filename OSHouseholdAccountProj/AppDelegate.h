//
//  AppDelegate.h
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

