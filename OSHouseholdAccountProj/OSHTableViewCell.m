//
//  OSHMainViewTableCell.m
//  CoreDataProj1
//
//  Created by sihon321 on 2017. 3. 27..
//  Copyright © 2017년 sihon321. All rights reserved.
//

#import "OSHTableViewCell.h"

@interface OSHTableViewCell ()

@end

static NSInteger const XpointtypeLabel = 10;
static NSInteger const YpointtypeLabel = 10;
static NSInteger const XpointcategoryLabel = 100;
static NSInteger const YpointcategoryLabel = 10;
static NSInteger const XpointmoneyLabel = 200;
static NSInteger const YpointmoneyLabel = 10;

static NSInteger const WidthLabel = 100;
static NSInteger const HeightLabel = 40;

@implementation OSHTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if(self){
        self.backgroundColor = [UIColor whiteColor];
        
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(XpointtypeLabel, YpointtypeLabel, WidthLabel, HeightLabel)];
        
        self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(XpointcategoryLabel, YpointcategoryLabel, WidthLabel, HeightLabel)];
        self.moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(XpointmoneyLabel, YpointmoneyLabel, WidthLabel, HeightLabel)];
        
        [self.contentView addSubview:self.typeLabel];
        [self.contentView addSubview:self.categoryLabel];
        [self.contentView addSubview:self.moneyLabel];
    }
    
    return self;

}

- (void) setCellDataFromType : (NSString *) type andCategory: (NSString *) category andMoney: (NSString *) money {
    [self.typeLabel setText:type];
    [self.categoryLabel setText:category];
    [self.moneyLabel setText:money];
}

@end
