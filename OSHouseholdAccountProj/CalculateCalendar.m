//
//  CalculateCalendar.m
//  CalendarProj
//
//  Created by sihon321 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "CalculateCalendar.h"

@interface CalculateCalendar ()
@property NSArray * weeksArray;
@property NSDateComponents *component;
@property NSCalendar *calendar;
@property NSDate* date;
@property NSString * weekStrings;

@end

@implementation CalculateCalendar

-(instancetype) init{
    self = [super init];
    if(self) {
        NSDate *date = [NSDate date];
        _calendar = [NSCalendar currentCalendar];
        _component = [_calendar components:NSCalendarUnitDay|NSCalendarUnitWeekday|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];

        [self initPresentValue:_component];
    }
    return self;
}

- (NSArray *) setupWeeksArray{
    _weeksArray = [NSArray arrayWithObjects: @"sun", @"mon", @"tue", @"wed", @"thu", @"fri", @"sat", nil];
    
    return _weeksArray;
}

- (void) loadCalendarFromYear: (NSInteger)year andMonth: (NSInteger)month{
    _component.day = 1;
    _component.month = month;
    _component.year = year;
    
    NSDate * newDate = [_calendar dateFromComponents:_component];
    
    NSDateComponents * convertComponents = [_calendar components:NSCalendarUnitDay|NSCalendarUnitWeekday|NSCalendarUnitDay|NSCalendarUnitYear fromDate:newDate];
    
    _moveWeekday = (NSInteger)convertComponents.weekday;
    _date = newDate;
}

- (NSInteger) getLastdayOfMonth{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange lastDay = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:_date];
    
    return (int)lastDay.length;
}

- (void) initPresentValue: (NSDateComponents*) component{
    _presentYear = _moveYear = component.year;
    _presentMonth = _moveMonth = component.month;
    _presentWeekday = _moveWeekday = component.weekday;
    _presentDay = _moveDay = component.day;
}

@end
