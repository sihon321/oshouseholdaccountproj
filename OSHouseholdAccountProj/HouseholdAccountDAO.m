//
//  HouseholdAccountDAO.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 31..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "HouseholdAccountDAO.h"

@interface HouseholdAccountDAO ()

@property NSMutableArray * allData;
@property NSString * filePath;
@property NSInteger monthMoney, todayMoney;

@end

@implementation HouseholdAccountDAO

- (void) loadHouseholdAccount{

    NSString * docPath = [NSHomeDirectory() stringByAppendingString:@"/AccountData.csv"];
    NSString * dataFile = [NSString stringWithContentsOfFile:docPath encoding:NSUTF8StringEncoding error:NULL];
    
    NSArray * RawData = [dataFile componentsSeparatedByString:@"\n"];
    
    NSArray *singleData = [NSArray array];
    _allData = [NSMutableArray array];
    
    NSInteger dataLength = RawData.count;
    NSLog(@"%ld %@", RawData.count, docPath);
    
    if(dataLength != 0){
       dataLength -= 1;
    }
    
    
    for(int i = 0; i<dataLength; i++){
        NSString * nextString = [NSString stringWithFormat:@"%@", RawData[i]];
        singleData = [nextString componentsSeparatedByString:@","];
        
        HouseholdAccountMO * data = [[HouseholdAccountMO alloc] initWithType:[singleData objectAtIndex:0]
                                                                     andDate:[self parseFromSTRFormat:[singleData objectAtIndex:1]]
                                                                 andClassify:[singleData objectAtIndex:2]
                                                                    andMoney:[singleData objectAtIndex:3]];
        
        [_allData addObject:data];
    }
}

- (NSMutableArray *) loadTodayHouseholdAccount : (NSDate *) today {
    
    NSMutableArray * returnDataArray = [NSMutableArray new];
    
    for(HouseholdAccountMO * data in _allData){
        NSString * now = [self parseFromDateFormat:today];
        NSString * day = [self parseFromDateFormat:data.selectedDay];

        if([now isEqualToString:day]){
            
            [returnDataArray addObject:data];
        }
    }
   
    if(returnDataArray.count == 0)
        return nil;
    else
        return returnDataArray;

}

- (NSMutableArray *) loadTodayMonthMoney : (NSDate *) today {
    NSMutableArray * returnDataArray = [NSMutableArray new];
    
    for(HouseholdAccountMO * data in _allData){
        NSString * arrData = [self parseFromDateFormat: today];
        NSArray * singleData = [arrData componentsSeparatedByString:@"-"];
        NSString * day = [NSString stringWithFormat:@"%@",data.selectedDay];
        NSArray * monthData = [day componentsSeparatedByString:@"-"];
        
        if(monthData.count > 1){
            if([[singleData objectAtIndex:1] isEqualToString:[monthData objectAtIndex:1]]){
            
                [returnDataArray addObject:data];
            }
        }
    }
    
    if(returnDataArray.count == 0)
        return nil;
    else
        return returnDataArray;
}

- (NSString *) parseFromDateFormat : (NSDate *) date {
    NSDateFormatter * dateFormatter = [NSDateFormatter new];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ko"];
    [dateFormatter setDateFormat:@"yyyy-MM-DD"];

    return [dateFormatter stringFromDate:date];
}

- (NSDate *) parseFromSTRFormat : (NSString *) dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ko"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
}

- (void) insertDataToDAO : (HouseholdAccountMO *) data {

    if(![data isEqual:nil])
        [_allData addObject:data];
}

- (void) saveHouseholdAccount {
    _filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"/AccountData.csv"];

    NSString * outputString = @"";
    
    for(HouseholdAccountMO * accountData in _allData){

        outputString = [outputString stringByAppendingFormat:@"%@,%@,%@,%@\n",accountData.type, accountData.selectedDay, accountData.classification, accountData.money];
    }
    
    [outputString writeToFile:_filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
}



@end
