//
//  OSHCollectionViewCell.h
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#ifndef OSHCollectionViewCell_h
#define OSHCollectionViewCell_h
#import <UIKit/UIKit.h>

@interface OSHCollectionViewCell : UICollectionViewCell{
    
}
@property (retain, nonatomic) UILabel* dayLabel;

- (void)setCellData:(NSString *)data;

@end


#endif /* OSHCollectionViewCell_h */
