//
//  main.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
