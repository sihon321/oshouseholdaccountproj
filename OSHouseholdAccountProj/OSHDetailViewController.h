//
//  OSHDetailViewController.h
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 28..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#ifndef OSHDetailViewController_h
#define OSHDetailViewController_h
#import <UIKit/UIKit.h>
#import "OSHTableViewCell.h"
#import "HouseholdAccountDAO.h"

@interface OSHDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    
}
@property NSInteger yearStr, monthStr, dayStr;

- (void) setupDate : (NSInteger) year Month : (NSInteger) month Day : (NSInteger) day;

@end
#endif /* OSHDetailViewController_h */
