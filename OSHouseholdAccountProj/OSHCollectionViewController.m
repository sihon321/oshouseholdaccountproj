//
//  OSHCollectionViewController.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 23..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "OSHCollectionViewController.h"
#import "CalculateCalendar.h"
#import "OSHDetailViewController.h"
#import "HouseholdAccountMO.h"
#import "HouseholdAccountDAO.h"

@interface OSHCollectionViewController ()

@property (strong, nonatomic) OSHDetailViewController * detailViewController;
@property (strong, nonatomic) UICollectionView * collectionView;
@property (strong, nonatomic) NSMutableArray * calendarArray;
@property (strong, nonatomic) UILabel * dateLabel, * categoryLabel, * moneyLabel;
@property (strong, nonatomic) UITextField *dateNumTextField, * categoryTextField, * moneyTextField;
@property (strong, nonatomic) UIDatePicker * datePicker;
@property (strong, nonatomic) UIButton *typeIncomeButton, * typeExpenseButton, * saveButton;
@property (strong, nonatomic) UIView * editView;

@property NSArray * accountArray;
@property CalculateCalendar * calculateCalendar;
@property NSInteger lastDay, blankDay;
@property NSInteger startDateSpacing, endDateSapcing;
@property HouseholdAccountDAO * accountDAO;

@end

static NSInteger const kNumberOfItemsInRows = 7;
static NSInteger const kNumberOfMonth = 12;
static NSInteger const kStartOfMonth = 1;
static CGFloat const kLineSpacing = 1.f;
static CGFloat const kCellHeightRatio = 1.0f;

static NSString * const kCellReuseIdentifier = @"OSHCellReuseIdentifier";
static NSString * const kShowDetailViewIdentifier = @"showDetailView";

static NSInteger const kNextPrevButtonXpoint = 0;
static NSInteger const kNextPrevButtonYpoint = 0;
static NSInteger const kNextPrevButtonWidth = 40;
static NSInteger const kNextPrevButtonHeight = 40;

static NSInteger const kSundayInterval = 0;
static NSInteger const kSaturdayInterval = 6;

static NSString * const kDateLabelStr = @"날짜";
static NSString * const kLocaleWithLocaleIdentifier = @"ko";
static NSString * const kCategoryLabelStr = @"분류";
static NSString * const kMoneyLabelStr = @"금액";
static NSString * const kEditButtonStr = @"저장";
static NSString * const kTypeIncomeButtonStr = @"수입";
static NSString * const kTypeExpenseButtonStr = @"지출";

@implementation OSHCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupCalculateCalendar];
    [self setupCalendar];
    [self setupNavigationButtonViews];
    [self setupCollectionView];
    [self setupEditView];
    [self setupConstraint];
    
    _startDateSpacing = kSundayInterval;
    _endDateSapcing = kSaturdayInterval;
    _accountDAO = [HouseholdAccountDAO new];
    
    [_accountDAO loadHouseholdAccount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup Methods

-(void) setupTabbarView{
    UIViewController * tabarView = [UIViewController new];
    
    NSMutableArray * tabViewControllers = [NSMutableArray new];
    [tabViewControllers addObject:tabarView];
    
}

- (void) setupCollectionView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = [NSString stringWithFormat:@"%ld / %ld",_calculateCalendar.presentYear, _calculateCalendar.presentMonth];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    CGSize mainSize = self.view.bounds.size;
    CGFloat itemWidth = (mainSize.width / kNumberOfItemsInRows) - kLineSpacing;
    CGFloat itemHeight = itemWidth * kCellHeightRatio;
    [flowLayout setItemSize:CGSizeMake(itemWidth, itemHeight)];
    [flowLayout setMinimumLineSpacing:kLineSpacing];
    [flowLayout setMinimumInteritemSpacing:kLineSpacing];
    
    _collectionView = [[UICollectionView alloc] initWithFrame:[self.view bounds] collectionViewLayout:flowLayout];
    [_collectionView registerClass:[OSHCollectionViewCell class] forCellWithReuseIdentifier:kCellReuseIdentifier];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:_collectionView];
}

- (void)setupNavigationButtonViews {
    UIButton *nextButton = [[UIButton alloc] init];
    
    [nextButton setFrame:CGRectMake(kNextPrevButtonXpoint, kNextPrevButtonYpoint, kNextPrevButtonWidth, kNextPrevButtonHeight)];
    [nextButton setTitle:@"next" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(reloadNextButtonTouched)
        forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *prevButton = [[UIButton alloc] init];
    [prevButton setFrame:CGRectMake(kNextPrevButtonXpoint, kNextPrevButtonYpoint, kNextPrevButtonWidth, kNextPrevButtonHeight)];
    [prevButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [prevButton setTitle:@"prev" forState:UIControlStateNormal];
    [prevButton addTarget:self action:@selector(reloadPrevButtonTouched)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *nextBarButton = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *prevBarButton = [[UIBarButtonItem alloc] initWithCustomView:prevButton];
    self.navigationItem.rightBarButtonItem = nextBarButton;
    self.navigationItem.leftBarButtonItem = prevBarButton;
}

- (void) setupEditView {
    _editView = [UIView new];
    _editView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_editView];
    
    _datePicker = [UIDatePicker new];
    _datePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setLocale:[NSLocale localeWithLocaleIdentifier: kLocaleWithLocaleIdentifier]];
    [_editView addSubview:_datePicker];

    _dateLabel = [UILabel new];
    _dateLabel.text = kDateLabelStr;
    _dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_editView addSubview:_dateLabel];

    _categoryLabel = [UILabel new];
    _categoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _categoryLabel.text = kCategoryLabelStr;
    [_editView addSubview:_categoryLabel];

    _moneyLabel = [UILabel new];
    _moneyLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _moneyLabel.text = kMoneyLabelStr;
    [_editView addSubview:_moneyLabel];

    _categoryTextField = [UITextField new];
    _categoryTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [_categoryTextField setKeyboardType:UIKeyboardTypeDefault];
    [_categoryTextField setBorderStyle:UITextBorderStyleLine];
    [_editView addSubview:_categoryTextField];
    
    _moneyTextField = [UITextField new];
    _moneyTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [_moneyTextField setKeyboardType:UIKeyboardTypeNumberPad];
    [_moneyTextField setBorderStyle:UITextBorderStyleLine];
    [_editView addSubview:_moneyTextField];
    
    _saveButton = [UIButton new];
    _saveButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_saveButton setTitle:kEditButtonStr forState:UIControlStateNormal];
    [_saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_saveButton addTarget:self action:@selector(saveWithType) forControlEvents:UIControlEventTouchUpInside];
    [_editView addSubview:_saveButton];

    _typeExpenseButton = [UIButton new];
    _typeExpenseButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_typeExpenseButton setTitle:kTypeExpenseButtonStr forState:UIControlStateNormal];
    [_typeExpenseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_typeExpenseButton addTarget:self action:@selector(selectedExpenseTypeButton) forControlEvents:UIControlEventTouchUpInside];
    [_editView addSubview:_typeExpenseButton];

    _typeIncomeButton = [UIButton new];
    _typeIncomeButton.translatesAutoresizingMaskIntoConstraints = NO;
    _typeIncomeButton.backgroundColor = [UIColor orangeColor];
    [_typeIncomeButton setTitle:kTypeIncomeButtonStr forState:UIControlStateNormal];
    [_typeIncomeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_typeIncomeButton addTarget:self action:@selector(selectedIncomeTypeButton) forControlEvents:UIControlEventTouchUpInside];
    [_editView addSubview:_typeIncomeButton];
}

- (void) setupConstraint {
    NSDictionary * viewsDictionary = @{@"editView":_editView, @"datePicker":_datePicker, @"dateLabel":_dateLabel, @"categoryLabel":_categoryLabel, @"moneyLabel":_moneyLabel, @"categoryTextField":_categoryTextField, @"moneyTextField":_moneyTextField, @"saveButton":_saveButton, @"typeExpenseButton":_typeExpenseButton, @"typeIncomeButton":_typeIncomeButton};
    NSDictionary *metrics = @{@"editHeight":@230,
                              @"vSpacing":@10,
                              @"hSpacing":@10,
                              @"eSpacing":@30,
                              @"dateLabelWidth":@50,
                              @"dateLabelHeight":@40,
                              @"datePickerHeight":@50,
                              @"datePickerWidth":@250,
                              @"saveButtonWidth":@50,
                              @"saveButtonHeight":@40,
                              @"categoryLabelWidth":@50,
                              @"categoryLabelHeight":@40,
                              @"categoryTextFieldWidth":@200,
                              @"categoryTextFieldHeight":@40,
                              @"moneyLabelWidth":@50,
                              @"moneyLabelHeight":@40,
                              @"moneyTextFieldWidth":@200,
                              @"moneyTextFieldHeight":@40,
                              @"typeExpenseHeight":@50,
                              @"typeExpenseWidth":@40,
                              @"typeIncomeHeight":@50,
                              @"typeIncomeWidth":@40};
    
    NSArray *edit_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[editView(editHeight)]"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:edit_constraint_V];
    
    NSArray *dateLabel_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[dateLabel(dateLabelWidth)]"
                                                                              options:0
                                                                              metrics:metrics
                                                                                views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:dateLabel_constraint_H];
    NSArray *dateLabel_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[dateLabel(dateLabelWidth)]"
                                                                              options:0
                                                                              metrics:metrics
                                                                                views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:dateLabel_constraint_V];
    NSArray *dateLabel_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[dateLabel]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:dateLabel_constraint_POS_V];
    NSArray *dateLabel_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[dateLabel]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:dateLabel_constraint_POS_H];

    
    NSArray *datePicker_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[datePicker(datePickerHeight)]"
                                                                              options:0
                                                                                metrics:metrics
                                                                                views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:datePicker_constraint_V];
    NSArray *datePicker_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[datePicker]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:datePicker_constraint_POS_V];
    NSArray *datePicker_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[dateLabel]-vSpacing-[datePicker]-vSpacing-[saveButton]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:datePicker_constraint_POS_H];
    
    
    NSArray *saveButton_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[saveButton(saveButtonWidth)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:saveButton_constraint_H];
    
    NSArray *saveButton_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[saveButton(saveButtonHeight)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:saveButton_constraint_V];
    
    NSArray *saveButton_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[saveButton]-hSpacing-|"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:saveButton_constraint_POS_H];
    
    NSArray *saveButton_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vSpacing-[saveButton]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:saveButton_constraint_POS_V];
    
    
    
    NSArray *categoryLabel_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[categoryLabel(categoryLabelWidth)]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryLabel_constraint_H];
    
    NSArray *categoryLabel_constraint_V= [NSLayoutConstraint constraintsWithVisualFormat:@"V:[categoryLabel(categoryLabelHeight)]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryLabel_constraint_V];
    
    NSArray *categoryLabel_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[categoryLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryLabel_constraint_POS_H];
    
    NSArray *categoryLabel_constraint_POS_V= [NSLayoutConstraint constraintsWithVisualFormat:@"V:[dateLabel]-eSpacing-[categoryLabel]"
                                                                                      options:0
                                                                                      metrics:metrics
                                                                                        views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryLabel_constraint_POS_V];
    

    NSArray *categoryTF_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[categoryTextField(categoryTextFieldHeight)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryTF_constraint_V];
    NSArray *categoryTF_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[categoryLabel]-hSpacing-[categoryTextField]-hSpacing-[typeIncomeButton]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryTF_constraint_POS_H];
    NSArray *categoryTF_constraint_POS_V= [NSLayoutConstraint constraintsWithVisualFormat:@"V:[datePicker]-eSpacing-[categoryTextField]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:categoryTF_constraint_POS_V];
    
    
    NSArray * moneyLabel_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[moneyLabel(moneyLabelWidth)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyLabel_constraint_H];
    
    NSArray * moneyLabel_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[moneyLabel(moneyLabelHeight)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyLabel_constraint_V];
    NSArray * moneyLabel_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[moneyLabel]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyLabel_constraint_POS_H];
    NSArray * moneyLabel_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[categoryLabel]-eSpacing-[moneyLabel]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyLabel_constraint_POS_V];
    
    NSArray * moneyTF_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[moneyTextField(moneyTextFieldHeight)]"
                                                                            options:0
                                                                            metrics:metrics
                                                                              views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyTF_constraint_V];
    NSArray * moneyTF_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[categoryTextField]-eSpacing-[moneyTextField]"
                                                                                 options:0
                                                                                 metrics:metrics
                                                                                   views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:moneyTF_constraint_POS_V];
     NSArray * moneyTF_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[categoryLabel]-hSpacing-[moneyTextField]-hSpacing-[typeExpenseButton]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
     [NSLayoutConstraint activateConstraints:moneyTF_constraint_POS_H];

    
    NSArray * typeIncomeBtn_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeIncomeButton(typeIncomeWidth)]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeIncomeBtn_constraint_H];
    NSArray * typeIncomeBtn_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[typeIncomeButton(typeIncomeHeight)]"
                                                                                  options:0
                                                                                  metrics:metrics
                                                                                    views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeIncomeBtn_constraint_V];
    NSArray * typeIncomeBtn_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeIncomeButton]-hSpacing-|"
                                                                                       options:0
                                                                                       metrics:metrics
                                                                                         views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeIncomeBtn_constraint_POS_H];
    NSArray * typeIncomeBtn_constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[saveButton]-eSpacing-[typeIncomeButton]"
                                                                                       options:0
                                                                                       metrics:metrics
                                                                                         views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeIncomeBtn_constraint_POS_V];
    NSArray * typeExpenseBtn_constraint_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeExpenseButton(typeExpenseWidth)]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeExpenseBtn_constraint_H];
    NSArray * typeExpenseBtn_constraint_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[typeExpenseButton(typeExpenseHeight)]"
                                                                                   options:0
                                                                                   metrics:metrics
                                                                                     views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeExpenseBtn_constraint_V];
    NSArray * typeExpenseBtn_constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeExpenseButton]-hSpacing-|"
                                                                                       options:0
                                                                                       metrics:metrics
                                                                                         views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeExpenseBtn_constraint_POS_H];
    NSArray * typeExpenseBtn_constraint_POS_V= [NSLayoutConstraint constraintsWithVisualFormat:@"V:[typeIncomeButton]-eSpacing-[typeExpenseButton]"
                                                                                       options:0
                                                                                       metrics:metrics
                                                                                         views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:typeExpenseBtn_constraint_POS_V];

    
    NSArray *constraint_POS_V = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[editView]-vSpacing-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:constraint_POS_V];
    
    NSArray *constraint_POS_H = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hSpacing-[editView]-hSpacing-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:viewsDictionary];
    [NSLayoutConstraint activateConstraints:constraint_POS_H];

}

- (void) setupCalendar {
    _calendarArray = [NSMutableArray new];
    NSArray * weeksArray = [_calculateCalendar setupWeeksArray];
    
    for(int i = 0; i<kNumberOfItemsInRows; ++i){
        [_calendarArray addObject: weeksArray[i]];
    }
    
    for(int i = 0; i<_blankDay; i++){
        [_calendarArray addObject: @" "];
    }
    
    for(int i = kStartOfMonth; i<=_lastDay; ++i){
        [_calendarArray addObject:[NSString stringWithFormat:@"%d", i]];
    }
}

- (void) setupCalculateCalendar {
    _calculateCalendar = [CalculateCalendar new];
    
    [_calculateCalendar loadCalendarFromYear: _calculateCalendar.presentYear andMonth: _calculateCalendar.presentMonth];
    
    _lastDay = [_calculateCalendar getLastdayOfMonth];
    _blankDay = _calculateCalendar.moveWeekday - 1; // weekday : sun - 0, sat - 6
}

- (void) reloadCollectionViewData{
    self.title = [NSString stringWithFormat:@"%ld / %ld", (long)_calculateCalendar.moveYear, (long)_calculateCalendar.moveMonth];
    [_calculateCalendar loadCalendarFromYear: _calculateCalendar.moveYear andMonth: _calculateCalendar.moveMonth];
    
    _lastDay = [_calculateCalendar getLastdayOfMonth];
    _blankDay = _calculateCalendar.moveWeekday - 1 ;
    
    [_calendarArray removeAllObjects];
    [self setupCalendar];
    
    [self.collectionView reloadData];
}

#pragma mark - Action Method

- (void) reloadNextButtonTouched {
    _calculateCalendar.moveMonth += 1;
    _startDateSpacing = kSundayInterval;
    _endDateSapcing = kSaturdayInterval;
    
    if(_calculateCalendar.moveMonth >kNumberOfMonth){
        _calculateCalendar.moveYear += 1;
        _calculateCalendar.moveMonth = kStartOfMonth;
    }
    
    [self reloadCollectionViewData];
}

- (void) reloadPrevButtonTouched {
    _calculateCalendar.moveMonth -= 1;
    _startDateSpacing = kSundayInterval;
    _endDateSapcing = kSaturdayInterval;
    
    if(_calculateCalendar.moveMonth < kStartOfMonth){
        _calculateCalendar.moveYear -= 1;
        _calculateCalendar.moveMonth = kNumberOfMonth;
    }
    
    [self reloadCollectionViewData];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger startDay = kStartOfMonth+_blankDay+kNumberOfItemsInRows-1;
    if(indexPath.row  >= startDay){
        NSInteger day = indexPath.row - kNumberOfItemsInRows -_blankDay + kStartOfMonth;
        
        _detailViewController = [OSHDetailViewController new];
        [_detailViewController setupDate:_calculateCalendar.moveYear Month: _calculateCalendar.moveMonth Day: day];
        
        [self.navigationController pushViewController:_detailViewController animated:YES];
    }
}

- (void) selectedExpenseTypeButton {
    [_typeExpenseButton setBackgroundColor:[UIColor orangeColor]];
    [_typeIncomeButton setBackgroundColor:[UIColor whiteColor]];
}

- (void) selectedIncomeTypeButton {
    [_typeExpenseButton setBackgroundColor:[UIColor whiteColor]];
    [_typeIncomeButton setBackgroundColor:[UIColor orangeColor]];
}

- (void) saveWithType{
    NSString * type = nil;
   
    if([_typeExpenseButton.backgroundColor isEqual:[UIColor orangeColor]]){
        type = kTypeExpenseButtonStr;
    }else {
        type = kTypeIncomeButtonStr;
    }
    
    if([[NSNumberFormatter alloc] numberFromString:_moneyTextField.text] == NULL){
        return;
    }
    
    HouseholdAccountMO * accountData = [[HouseholdAccountMO alloc] initWithType:type andDate:_datePicker.date andClassify:_categoryTextField.text andMoney:[[NSNumberFormatter alloc] numberFromString:_moneyTextField.text]];

    [_accountDAO insertDataToDAO:accountData];
    [_accountDAO saveHouseholdAccount];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _lastDay+kNumberOfItemsInRows+_blankDay;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OSHCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [OSHCollectionViewCell new];
    }
    
    [cell setCellData:[NSString stringWithFormat:@"%@", _calendarArray[(NSInteger)indexPath.row]]];
    [self setLableColoring:cell index:indexPath.row];
    
    return cell;
}

- (void) setLableColoring : (OSHCollectionViewCell*) cell index: (NSInteger) row{
    if((NSInteger)row == _startDateSpacing){
        [cell.dayLabel setTextColor:[UIColor redColor]];
        _startDateSpacing += kNumberOfItemsInRows;
    }else if((NSInteger)row == _endDateSapcing) {
        [cell.dayLabel setTextColor:[UIColor blueColor]];
        _endDateSapcing += kNumberOfItemsInRows;
    }else {
        [cell.dayLabel setTextColor:[UIColor blackColor]];
    }
    
    NSInteger startDay = _calculateCalendar.moveDay+_blankDay+kNumberOfItemsInRows-1;
    
    if(((NSInteger)row == startDay)
       && (_calculateCalendar.moveMonth == _calculateCalendar.presentMonth)
       && (_calculateCalendar.moveYear == _calculateCalendar.presentYear)){
        
        [cell setBackgroundColor:[UIColor orangeColor]];
    } else {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
}

@end
