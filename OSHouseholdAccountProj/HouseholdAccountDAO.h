//
//  HouseholdAccountDAO.h
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 31..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#ifndef HouseholdAccountDAO_h
#define HouseholdAccountDAO_h
#import <Foundation/Foundation.h>
#import "HouseholdAccountMO.h"

@interface HouseholdAccountDAO : NSObject{
    
}

- (void) loadHouseholdAccount;
- (NSMutableArray *) loadTodayHouseholdAccount : (NSDate *) today;
- (void) insertDataToDAO : (HouseholdAccountMO *) data;
- (void) saveHouseholdAccount;
- (NSMutableArray *) loadTodayMonthMoney : (NSDate *) today;

@end

#endif /* HouseholdAccountDAO_h */
