//
//  HouseholdAccountMO.m
//  OSHouseholdAccountProj
//
//  Created by sihon312 on 2017. 3. 30..
//  Copyright © 2017년 sihon312. All rights reserved.
//

#import "HouseholdAccountMO.h"

@interface HouseholdAccountMO ()

@end

@implementation HouseholdAccountMO

- (id) initWithType: (NSString *) type andDate: (NSDate *) day andClassify: (NSString *) category andMoney: (NSNumber *) money{
    self = [super init];
    
    if(self){
    _type = type;
    _selectedDay = day;
    _classification = category;
    _money = money;
    }
    return self;
}

@end
